package com.milankas.training.userapi.services;

import com.milankas.training.userapi.dto.in.NewPassword;
import com.milankas.training.userapi.dto.in.UserInDto;
import com.milankas.training.userapi.dto.log.LogStatusDto;
import com.milankas.training.userapi.dto.out.UserDto;
import com.milankas.training.userapi.dto.patch.UserPatchDto;
import com.milankas.training.userapi.mappers.PasswordMapper;
import com.milankas.training.userapi.mappers.UserMapper;
import com.milankas.training.userapi.persistance.model.AuthenticationRequest;
import com.milankas.training.userapi.persistance.model.Password;
import com.milankas.training.userapi.persistance.model.User;
import com.milankas.training.userapi.persistance.repository.PasswordRepository;
import com.milankas.training.userapi.persistance.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    PasswordRepository passwordRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    PasswordMapper passwordMapper;

    @Mock
    UserMapper userMapper;

    @InjectMocks
    UserService userService;

    UserDto userDto = new UserDto();

    UUID userId;
    String firstName;
    String lastName;
    String email;

    User userToSend = new User();
    Password password = new Password();

    UUID passwordId;
    String hash;
    String salt;
    int status;

    @BeforeEach
    void setUp() {
        userId = UUID.fromString("a791b7c7-baa9-4a0f-b3e9-ca024fd54478");
        firstName = "Andres";
        lastName = "Arispe";
        email = "andres.123@gmail.com";

        passwordId = UUID.fromString("a800b8c8-baa9-4a0f-b3e9-ca024fd54478");
        hash = "8743b52063cd84097a65d1633f5c74f5";
        salt = "d63d0e21fdc05f618d55ef306c54af82:13288442151473";
        status = 1;
        password.setHash(hash);
        password.setSalt(salt);
        password.setStatus(status);
        password.setId(passwordId);
    }

    @Test
    void getUsers() {
        List<UserDto> userDtos = new ArrayList<>();
        userDtos.add(userDto);
        List<User> users = new ArrayList<>();
        when(userMapper.toUsersDto(any())).thenReturn(userDtos);
        List<UserDto> userDtosTest = userService.getUsers();
        assertEquals(userDtos, userDtosTest);
    }

    @Test
    void getUser() {
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        userToSend.setPasswords(passwords);
        when(userRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(userToSend));
        when(userMapper.userToDto(any())).thenReturn(userDto);
        UserDto userDtoToTest = userService.getUser(userId);
        assertEquals(userDto, userDtoToTest);
    }

    @Test
    void saveUser() {
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        UserInDto userInDto = new UserInDto();
        userInDto.setId(userId);
        when(userMapper.userToDto(any())).thenReturn(userDto);
        UserDto userDtoToTest = userService.saveUser(userInDto);
        verify(userRepository,times(1)).save(any());
        assertEquals(userDto, userDtoToTest);
    }

    @Test
    void validEmail() {
        List<User> users = new ArrayList<>();
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        userToSend.setPasswords(passwords);
        userToSend.setEmail(email);
        users.add(userToSend);
        Boolean existUser = userService.validEmail("andres.123@gmail.com");
        assertEquals(true, existUser);
    }

    @Test
    void deleteExistingUser() {
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        userToSend.setPasswords(passwords);
        when(userRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(userToSend));
        when(userMapper.userToDto(any())).thenReturn(userDto);
        UserDto userDtoToTest = userService.deleteUser(userId);
        assertEquals(userDto, userDtoToTest);
    }

    @Test
    void updateUser() {
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        userToSend.setPasswords(passwords);
        UserPatchDto userPatchDto = new UserPatchDto();
        userPatchDto.setId(userId);
        when(userRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(userToSend));
        when(userMapper.userToDto(any())).thenReturn(userDto);
        UserDto userDtoToTest = userService.updateUser(userId, userPatchDto);
        assertEquals(userDto, userDtoToTest);
    }

    @Test
    void correctUser() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setPassword("andres.123");
        authenticationRequest.setUsername("enrique.123@gmail.com");
        List<User> users = new ArrayList<>();
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        userToSend.setPasswords(passwords);
        userToSend.setFirstName(firstName);
        userToSend.setLastName(lastName);
        userToSend.setEmail(email);
        users.add(userToSend);
        when(userRepository.findAll()).thenReturn(users);
        Boolean existUser = userService.correctUser(authenticationRequest);
        assertEquals(false , existUser);
    }

    @Test
    void updateIncorrectPassword() {
        NewPassword newPasswordTest = new NewPassword();
        newPasswordTest.setActualPassword("andres.123");
        newPasswordTest.setNewPassword("enrique.123");
        List<Password> passwords = new ArrayList<>();
        passwords.add(password);
        userToSend.setId(userId);
        userToSend.setPasswords(passwords);
        userToSend.setFirstName(firstName);
        userToSend.setLastName(lastName);
        userToSend.setEmail(email);
        when(userRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(userToSend));
        String test = userService.updatePassword(userId, newPasswordTest);
        verify(userRepository, times(0)).save(any());
        assertEquals("Actual password it's incorrect", test);
    }

    @Test
    void updateCorrectPassword() {
        User comprobeUser = new User();
        comprobeUser.setId(userId);
        comprobeUser.setFirstName(firstName);
        comprobeUser.setLastName(lastName);
        comprobeUser.setEmail(email);

        NewPassword newPasswordTest = new NewPassword();
        newPasswordTest.setActualPassword("enrique.123");
        newPasswordTest.setNewPassword("andres.123");

        Password comprobePassword = new Password();
        List<Password> comprobeListPasswords = new ArrayList<>();
        comprobePassword.setId(passwordId);
        comprobePassword.setSalt("e8c025a1bf0ef2aa");
        comprobePassword.setHash("24843acc046fe23e84886793dbf4250aa127bde10b280de1ccf17d837d4525d4");
        comprobePassword.setStatus(1);
        comprobeListPasswords.add(comprobePassword);
        comprobeUser.setPasswords(comprobeListPasswords);

        when(userRepository.findById(any())).thenReturn(java.util.Optional.of(comprobeUser));
        when(userRepository.save(any())).thenReturn(comprobeUser);
        String test = userService.updatePassword(userId, newPasswordTest);
        verify(userRepository, times(1)).save(any());
        assertEquals("Password changed", test);
    }

    @Test
    void ComprobeCorrectPassword() {
        User comprobeUser = new User();
        Password comprobePassword = new Password();
        List<Password> comprobeListPasswords = new ArrayList<>();
        comprobePassword.setId(passwordId);
        comprobePassword.setSalt("e8c025a1bf0ef2aa");
        comprobePassword.setHash("24843acc046fe23e84886793dbf4250aa127bde10b280de1ccf17d837d4525d4");
        comprobePassword.setStatus(1);
        comprobeListPasswords.add(comprobePassword);
        comprobeUser.setId(userId);
        comprobeUser.setFirstName(firstName);
        comprobeUser.setLastName(lastName);
        comprobeUser.setEmail(email);
        comprobeUser.setPasswords(comprobeListPasswords);

        boolean comprobe = userService.comprobePassword(comprobeUser, "enrique.123");
        assertEquals(true, comprobe);
    }

    @Test
    void ComprobeIncorrectPassword() {
        User comprobeUser = new User();
        Password comprobePassword = new Password();
        List<Password> comprobeListPasswords = new ArrayList<>();
        comprobePassword.setId(passwordId);
        comprobePassword.setSalt("e8c025a1bf0ef2aa");
        comprobePassword.setHash("24843acc046fe23e84886793dbf4250aa127bde10b280de1ccf17d837d4525d4");
        comprobePassword.setStatus(1);
        comprobeListPasswords.add(comprobePassword);
        comprobeUser.setId(userId);
        comprobeUser.setFirstName(firstName);
        comprobeUser.setLastName(lastName);
        comprobeUser.setEmail(email);
        comprobeUser.setPasswords(comprobeListPasswords);

        boolean comprobe = userService.comprobePassword(comprobeUser, "andres.123");
        assertEquals(false, comprobe);
    }

    @Test
    void ComprobeLogStatusDto() {
        LogStatusDto logStatusDto = userService.buildLogStatusDto("info", "get Users");
        LogStatusDto logStatusDtoTest = new LogStatusDto();
        logStatusDtoTest.setDateAndTime(logStatusDto.getDateAndTime());
        logStatusDtoTest.setMessage("get Users");
        logStatusDtoTest.setLevel("info");
        logStatusDtoTest.setServiceName("User-API");
        assertEquals(logStatusDtoTest.toString(), logStatusDto.toString());
    }

}