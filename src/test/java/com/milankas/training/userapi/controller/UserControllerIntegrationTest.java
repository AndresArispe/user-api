package com.milankas.training.userapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.milankas.training.userapi.dto.in.NewPassword;
import com.milankas.training.userapi.dto.in.UserInDto;
import com.milankas.training.userapi.dto.out.UserDto;
import com.milankas.training.userapi.dto.patch.UserPatchDto;
import com.milankas.training.userapi.utils.JwtUtils;
import com.milankas.training.userapi.persistance.model.AuthenticationRequest;
import com.milankas.training.userapi.persistance.model.Password;
import com.milankas.training.userapi.persistance.model.User;
import com.milankas.training.userapi.services.MyUserDetailsService;
import com.milankas.training.userapi.services.UserService;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class UserControllerIntegrationTest {

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    UserService userService;

    @Mock
    RestTemplate restTemplate;

    @Mock
    MyUserDetailsService userDetailsService;

    @Mock
    JwtUtils jwtUtils;

    @Mock
    RabbitTemplate template;

    @InjectMocks
    UserController userController;

    MockMvc mockMvc;

    UserDto userDto = new UserDto();
    User userToSend = new User();
    Password password = new Password();

    UUID userId;
    String firstName;
    String lastName;
    String email;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .build();

        userId = UUID.fromString("a791b7c7-baa9-4a0f-b3e9-ca024fd54478");
        firstName = "Andres";
        lastName = "Arispe";
        email = "andres.123@gmail.com";

        userDto.setId(userId);
        userDto.setEmail(email);
        userDto.setFirstName(firstName);
        userDto.setLastName(lastName);

    }

    @Test
    void getAllUsers() throws Exception {
        List<UserDto> userDtos = new ArrayList<>();
        userDtos.add(userDto);
        when(userService.getUsers()).thenReturn(userDtos);

        MvcResult result = mockMvc.perform(get("/v1/users"))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals(content,"[{\"id\":\"a791b7c7-baa9-4a0f-b3e9-ca024fd54478\",\"firstName\":\"Andres\",\"lastName\":\"Arispe\",\"email\":\"andres.123@gmail.com\"}]");
        verify(userService, times(1)).getUsers();
    }

    @Test
    void getUserById() throws Exception {
        when(userService.getUser(any())).thenReturn(userDto);

        MvcResult result = mockMvc.perform(get("/v1/users/a791b7c7-baa9-4a0f-b3e9-ca024fd54478"))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals(content,"{\"id\":\"a791b7c7-baa9-4a0f-b3e9-ca024fd54478\",\"firstName\":\"Andres\",\"lastName\":\"Arispe\",\"email\":\"andres.123@gmail.com\"}");
        verify(userService, times(2)).getUser(any());
    }

    @Test
    void healthCheck() throws Exception {
        MvcResult result = mockMvc.perform(get("/v1/users/healthcheck"))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals(content,"I'm Alive");
    }

    @Test
    void postUser() throws Exception {
        UserInDto userInDto = new UserInDto();
        userInDto.setEmail(email);
        userInDto.setFirstName(firstName);
        userInDto.setLastName(lastName);
        userInDto.setPassword("andres.123");

        when(userService.validEmail(any())).thenReturn(true);
        when(userService.saveUser(any())).thenReturn(userDto);

        MvcResult result = mockMvc.perform(post("/v1/users")
                .content(asJsonString(userInDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        assertEquals("{\"id\":\"a791b7c7-baa9-4a0f-b3e9-ca024fd54478\",\"firstName\":\"Andres\",\"lastName\":\"Arispe\",\"email\":\"andres.123@gmail.com\"}",content);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void createAuthenticationToken() throws Exception {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUsername("andres.123@gmail.com");
        authenticationRequest.setPassword("andres.123");

        when(userService.correctUser(any())).thenReturn(true);

        MvcResult result = mockMvc.perform(post("/v1/users/authenticate")
                .content(asJsonString(authenticationRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        verify(jwtUtils, times(1)).generateToken(any());
        verify(userDetailsService, times(1)).loadUserByUsername(any());
    }

    @Test
    void deleteUser() throws Exception {
        when(userService.deleteUser(any())).thenReturn(userDto);

        ResultActions result = mockMvc.perform(delete("/v1/users/a791b7c7-baa9-4a0f-b3e9-ca024fd54478"))
                .andExpect(status().isNoContent());

        verify(userService, times(1)).deleteUser(any());
    }

    @Test
    void patchUser() throws Exception {
        UserPatchDto userPatchDto = new UserPatchDto();
        userPatchDto.setId(userId);
        userPatchDto.setFirstName(firstName);
        userPatchDto.setLastName(lastName);
        userPatchDto.setEmail(email);
        userPatchDto.setPassword("andres.123");

        when(userService.getUser(any())).thenReturn(userDto);
        when(userService.updateUser(any(), any())).thenReturn(userDto);

        MvcResult result = mockMvc.perform(patch("/v1/users/a791b7c7-baa9-4a0f-b3e9-ca024fd54478")
                .content(asJsonString(userPatchDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals("{\"id\":\"a791b7c7-baa9-4a0f-b3e9-ca024fd54478\",\"firstName\":\"Andres\",\"lastName\":\"Arispe\",\"email\":\"andres.123@gmail.com\"}", content);
    }

    @Test
    void changePassword() throws Exception {
        NewPassword newPassword = new NewPassword();
        newPassword.setNewPassword("enrique.123");
        newPassword.setActualPassword("andres.123");

        JSONObject message = new JSONObject();
        message.put("message", "Password changed");

        when(userService.getUser(any())).thenReturn(userDto);
        when(userService.updatePassword(any(), any())).thenReturn("Password changed");

        MvcResult result = mockMvc.perform(patch("/v1/users/a791b7c7-baa9-4a0f-b3e9-ca024fd54478/change-password")
                .content(asJsonString(newPassword))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        assertEquals(message.toString(), content);

    }
}