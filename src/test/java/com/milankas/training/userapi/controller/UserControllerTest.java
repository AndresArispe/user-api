package com.milankas.training.userapi.controller;

import com.milankas.training.userapi.dto.in.NewPassword;
import com.milankas.training.userapi.dto.in.UserInDto;
import com.milankas.training.userapi.dto.out.UserDto;
import com.milankas.training.userapi.dto.patch.UserPatchDto;
import com.milankas.training.userapi.utils.JwtUtils;
import com.milankas.training.userapi.persistance.model.AuthenticationRequest;
import com.milankas.training.userapi.persistance.model.Password;
import com.milankas.training.userapi.persistance.model.User;
import com.milankas.training.userapi.services.MyUserDetailsService;
import com.milankas.training.userapi.services.UserService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.client.RestTemplate;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    UserService userService;

    @Mock
    RestTemplate restTemplate;

    @Mock
    MyUserDetailsService userDetailsService;

    @Mock
    JwtUtils jwtUtils;

    @Mock
    RabbitTemplate template;

    @InjectMocks
    UserController userController;

    BindingResult resultWithErrors = new BindingResult() {
        @Override
        public Object getTarget() {
            return null;
        }

        @Override
        public Map<String, Object> getModel() {
            return null;
        }

        @Override
        public Object getRawFieldValue(String s) {
            return null;
        }

        @Override
        public PropertyEditor findEditor(String s, Class<?> aClass) {
            return null;
        }

        @Override
        public PropertyEditorRegistry getPropertyEditorRegistry() {
            return null;
        }

        @Override
        public String[] resolveMessageCodes(String s) {
            return new String[0];
        }

        @Override
        public String[] resolveMessageCodes(String s, String s1) {
            return new String[0];
        }

        @Override
        public void addError(ObjectError objectError) {

        }

        @Override
        public String getObjectName() {
            return null;
        }

        @Override
        public void setNestedPath(String s) {

        }

        @Override
        public String getNestedPath() {
            return null;
        }

        @Override
        public void pushNestedPath(String s) {

        }

        @Override
        public void popNestedPath() throws IllegalStateException {

        }

        @Override
        public void reject(String s) {

        }

        @Override
        public void reject(String s, String s1) {

        }

        @Override
        public void reject(String s, Object[] objects, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1, String s2) {

        }

        @Override
        public void rejectValue(String s, String s1, Object[] objects, String s2) {

        }

        @Override
        public void addAllErrors(Errors errors) {

        }

        @Override
        public boolean hasErrors() {
            return true;
        }

        @Override
        public int getErrorCount() {
            return 0;
        }

        @Override
        public List<ObjectError> getAllErrors() {
            List<ObjectError> errors = new ArrayList<>();
            ObjectError error = new ObjectError("Bad Dto", "Validation Failure");
            errors.add(error);
            return errors;
        }

        @Override
        public boolean hasGlobalErrors() {
            return false;
        }

        @Override
        public int getGlobalErrorCount() {
            return 0;
        }

        @Override
        public List<ObjectError> getGlobalErrors() {
            return null;
        }

        @Override
        public ObjectError getGlobalError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors() {
            return false;
        }

        @Override
        public int getFieldErrorCount() {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors() {
            return null;
        }

        @Override
        public FieldError getFieldError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors(String s) {
            return false;
        }

        @Override
        public int getFieldErrorCount(String s) {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors(String s) {
            return null;
        }

        @Override
        public FieldError getFieldError(String s) {
            return null;
        }

        @Override
        public Object getFieldValue(String s) {
            return null;
        }

        @Override
        public Class<?> getFieldType(String s) {
            return null;
        }
    };

    BindingResult resultWithoutErrors = new BindingResult() {
        @Override
        public Object getTarget() {
            return null;
        }

        @Override
        public Map<String, Object> getModel() {
            return null;
        }

        @Override
        public Object getRawFieldValue(String s) {
            return null;
        }

        @Override
        public PropertyEditor findEditor(String s, Class<?> aClass) {
            return null;
        }

        @Override
        public PropertyEditorRegistry getPropertyEditorRegistry() {
            return null;
        }

        @Override
        public String[] resolveMessageCodes(String s) {
            return new String[0];
        }

        @Override
        public String[] resolveMessageCodes(String s, String s1) {
            return new String[0];
        }

        @Override
        public void addError(ObjectError objectError) {

        }

        @Override
        public String getObjectName() {
            return null;
        }

        @Override
        public void setNestedPath(String s) {

        }

        @Override
        public String getNestedPath() {
            return null;
        }

        @Override
        public void pushNestedPath(String s) {

        }

        @Override
        public void popNestedPath() throws IllegalStateException {

        }

        @Override
        public void reject(String s) {

        }

        @Override
        public void reject(String s, String s1) {

        }

        @Override
        public void reject(String s, Object[] objects, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1) {

        }

        @Override
        public void rejectValue(String s, String s1, String s2) {

        }

        @Override
        public void rejectValue(String s, String s1, Object[] objects, String s2) {

        }

        @Override
        public void addAllErrors(Errors errors) {

        }

        @Override
        public boolean hasErrors() {
            return false;
        }

        @Override
        public int getErrorCount() {
            return 1;
        }

        @Override
        public List<ObjectError> getAllErrors() {
            return null;
        }

        @Override
        public boolean hasGlobalErrors() {
            return false;
        }

        @Override
        public int getGlobalErrorCount() {
            return 0;
        }

        @Override
        public List<ObjectError> getGlobalErrors() {
            return null;
        }

        @Override
        public ObjectError getGlobalError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors() {
            return false;
        }

        @Override
        public int getFieldErrorCount() {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors() {
            return null;
        }

        @Override
        public FieldError getFieldError() {
            return null;
        }

        @Override
        public boolean hasFieldErrors(String s) {
            return false;
        }

        @Override
        public int getFieldErrorCount(String s) {
            return 0;
        }

        @Override
        public List<FieldError> getFieldErrors(String s) {
            return null;
        }

        @Override
        public FieldError getFieldError(String s) {
            return null;
        }

        @Override
        public Object getFieldValue(String s) {
            return null;
        }

        @Override
        public Class<?> getFieldType(String s) {
            return null;
        }
    };

    MockMvc mockMvc;

    UserDto userDto = new UserDto();
    User userToSend = new User();
    Password password = new Password();

    UUID userId;
    String firstName;
    String lastName;
    String email;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .build();

        userId = UUID.fromString("a791b7c7-baa9-4a0f-b3e9-ca024fd54478");
        firstName = "Andres";
        lastName = "Arispe";
        email = "andres.123@gmail.com";

        userDto.setId(userId);
        userDto.setEmail(email);
        userDto.setFirstName(firstName);
        userDto.setLastName(lastName);

    }

    @Test
    void getAllUsers() {
        List<UserDto> userDtos = new ArrayList<>();
        userDtos.add(userDto);
        when(userService.getUsers()).thenReturn(userDtos);
        List<UserDto> userDtosTest = userController.getAllUsers();
        assertEquals(userDtos, userDtosTest);
    }

    @Test
    void getUserById() {
        when(userService.getUser(any())).thenReturn(userDto);
        UserDto userDtoTest = userController.getUserById(userId);
        assertEquals(userDto, userDtoTest);
    }

    @Test
    void healthCheck() {
        String content = userController.healthCheck();
        assertEquals("I'm Alive", content);
    }

    @Test
    void postUserWithoutErrors() {
        UserInDto userInDto = new UserInDto();
        userInDto.setEmail(email);
        userInDto.setFirstName(firstName);
        userInDto.setLastName(lastName);
        userInDto.setPassword("andres.123");

        when(userService.validEmail(any())).thenReturn(true);
        when(userService.saveUser(any())).thenReturn(userDto);

        ResponseEntity<Object> order = userController.postUser(userInDto, resultWithoutErrors);
        ResponseEntity<Object> test = new ResponseEntity<>(userDto, HttpStatus.OK);

        assertEquals(200, order.getStatusCodeValue());
        assertEquals(order, test);
    }

    @Test
    void postUserWithErrors() {
        UserInDto userInDto = new UserInDto();
        userInDto.setEmail(email);
        userInDto.setFirstName(firstName);
        userInDto.setLastName(lastName);
        userInDto.setPassword("andres.123");

        ResponseEntity<Object> order = userController.postUser(userInDto, resultWithErrors);

        assertEquals(400, order.getStatusCodeValue());
    }

    @Test
    void postUserWithDuplicatedEmail() {
        UserInDto userInDto = new UserInDto();
        userInDto.setEmail(email);
        userInDto.setFirstName(firstName);
        userInDto.setLastName(lastName);
        userInDto.setPassword("andres.123");

        when(userService.validEmail(any())).thenReturn(false);
        ResponseEntity<Object> order = userController.postUser(userInDto, resultWithoutErrors);

        assertEquals(400, order.getStatusCodeValue());
    }

    @Test
    void createValidAuthenticationToken() throws Exception {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUsername("andres.123@gmail.com");
        authenticationRequest.setPassword("andres.123");

        String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhbmRyZXMuMTIzNEBnbWFpbC5jb20iLCJpYXQiOjE2MDUxMjE1MjEsImV4cCI6MTYwNTE1NzUyMSwianRpIjoiOGRmOWFjMDYtYjlhZi00ZTAxLThmY2QtNzMxOTU5ZjU3MjhlIiwiaXNzIjoibG9jYWxob3N0OjgwODUvdjEvdXNlcnMvYXV0aGVudGljYXRlIn0.XTAeMRvKsiqMrBcn94hrsFcsKXxFZF-D5NuRq86RjlhTaFJhnUTL212ka7KwImQrItQJV3Yq46hscAw2GfzGAdEiDtyWAH_px304GkHH1R6OUm4IhaRacqmfzxNOTPRibI-K7p2hrWjMXEdrAk5EkqRLXVa5x-p-2nkpNRVuUbY493VFaK_HyyMrXb-Fbz93N_IkP2vDdkhjr_keVwln-tzOybGTZ_QCl6KUzK8iuLZkgX-fCJi2bhjF3urLKAM8ODG3bvSZcj7RIc8pXv7TNffUT9fy3sY1o2bkM6unyzt7FsdbBv5STDxytw3iBbsfDc_fmQuQsmRzJa5jIspSLQ";

        when(userService.correctUser(any())).thenReturn(true);
        when(jwtUtils.generateToken(any())).thenReturn(jwt);

        ResponseEntity<?> order = userController.createAuthenticationToken(authenticationRequest);
        assertEquals(200, order.getStatusCodeValue());
        verify(jwtUtils, times(1)).generateToken(any());
    }

    @Test
    void createInvalidAuthenticationToken() throws Exception {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUsername("andres.123@gmail.com");
        authenticationRequest.setPassword("andres.123");

        String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhbmRyZXMuMTIzNEBnbWFpbC5jb20iLCJpYXQiOjE2MDUxMjE1MjEsImV4cCI6MTYwNTE1NzUyMSwianRpIjoiOGRmOWFjMDYtYjlhZi00ZTAxLThmY2QtNzMxOTU5ZjU3MjhlIiwiaXNzIjoibG9jYWxob3N0OjgwODUvdjEvdXNlcnMvYXV0aGVudGljYXRlIn0.XTAeMRvKsiqMrBcn94hrsFcsKXxFZF-D5NuRq86RjlhTaFJhnUTL212ka7KwImQrItQJV3Yq46hscAw2GfzGAdEiDtyWAH_px304GkHH1R6OUm4IhaRacqmfzxNOTPRibI-K7p2hrWjMXEdrAk5EkqRLXVa5x-p-2nkpNRVuUbY493VFaK_HyyMrXb-Fbz93N_IkP2vDdkhjr_keVwln-tzOybGTZ_QCl6KUzK8iuLZkgX-fCJi2bhjF3urLKAM8ODG3bvSZcj7RIc8pXv7TNffUT9fy3sY1o2bkM6unyzt7FsdbBv5STDxytw3iBbsfDc_fmQuQsmRzJa5jIspSLQ";

        when(userService.correctUser(any())).thenReturn(false);

        ResponseEntity<?> order = userController.createAuthenticationToken(authenticationRequest);
        assertEquals(403, order.getStatusCodeValue());
        verify(jwtUtils, times(0)).generateToken(any());
    }

    @Test
    void deleteUser() {
        when(userService.deleteUser(any())).thenReturn(userDto);
        userController.deleteUser(userId);
        verify(userService, times(1)).deleteUser(any());
    }

    @Test
    void patchUser() {
        UserPatchDto userPatchDto = new UserPatchDto();
        userPatchDto.setId(userId);
        userPatchDto.setFirstName(firstName);
        userPatchDto.setLastName(lastName);
        userPatchDto.setEmail(email);
        userPatchDto.setPassword("andres.123");

        when(userService.getUser(any())).thenReturn(userDto);
        when(userService.updateUser(any(), any())).thenReturn(userDto);

        ResponseEntity<Object> userDtoTest = userController.patchUser(userId, userPatchDto, resultWithoutErrors);
        ResponseEntity<Object> test = new ResponseEntity<>(userDto, HttpStatus.OK);

        assertEquals(200, userDtoTest.getStatusCodeValue());
        assertEquals(userDtoTest, test);
    }

    @Test
    void changePasswordWithCorrectValues() throws JSONException {
        NewPassword newPassword = new NewPassword();
        newPassword.setNewPassword("enrique.123");
        newPassword.setActualPassword("andres.123");

        JSONObject message = new JSONObject();
        message.put("message", "Password changed");

        when(userService.getUser(any())).thenReturn(userDto);
        when(userService.updatePassword(any(), any())).thenReturn("Password changed");

        ResponseEntity<?> userDtoTest = userController.changePassword(userId, newPassword, resultWithoutErrors);
        ResponseEntity<?> test = new ResponseEntity<>(message.toString(), HttpStatus.OK);

        assertEquals(200, userDtoTest.getStatusCodeValue());
        assertEquals(userDtoTest, test);
    }

    @Test
    void changePasswordWithIncorrectValues() {
        NewPassword newPassword = new NewPassword();
        newPassword.setNewPassword("enrique.123");
        newPassword.setActualPassword("andres.123");

        ResponseEntity<?> userDtoTest = userController.changePassword(userId, newPassword, resultWithErrors);

        assertEquals(400, userDtoTest.getStatusCodeValue());
    }

    @Test
    void changePasswordWithInvalidUser() {
        NewPassword newPassword = new NewPassword();
        newPassword.setNewPassword("enrique.123");
        newPassword.setActualPassword("andres.123");

        when(userService.getUser(any())).thenReturn(null);

        ResponseEntity<?> userDtoTest = userController.changePassword(userId, newPassword, resultWithoutErrors);

        assertEquals(404, userDtoTest.getStatusCodeValue());
    }
}