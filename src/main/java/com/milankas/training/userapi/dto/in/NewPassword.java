package com.milankas.training.userapi.dto.in;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
public class NewPassword {

    @NotNull(message = "Actual password it's required")
    String actualPassword;

    @NotNull(message = "New Password it's required")
    @Size(min = 8, max = 80, message = "New password must be have 8 characters minimum and max 80 characters")
    String newPassword;
}
