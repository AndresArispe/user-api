package com.milankas.training.userapi.dto.log;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class LogStatusDto {

    private String level;
    private Date dateAndTime;
    private String serviceName;
    private String message;

    @Override
    public String toString() {
        return "LogStatusDto{" +
                "level='" + level + '\'' +
                ", dateAndTime=" + dateAndTime +
                ", serviceName='" + serviceName + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
