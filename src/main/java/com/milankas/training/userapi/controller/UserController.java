package com.milankas.training.userapi.controller;

import com.milankas.training.userapi.dto.in.NewPassword;
import com.milankas.training.userapi.dto.in.UserInDto;
import com.milankas.training.userapi.dto.log.LogStatusDto;
import com.milankas.training.userapi.dto.out.UserDto;
import com.milankas.training.userapi.dto.patch.UserPatchDto;
import com.milankas.training.userapi.errors.ErrorResponse;
import com.milankas.training.userapi.utils.JwtUtils;
import com.milankas.training.userapi.message.MessagingConfig;
import com.milankas.training.userapi.persistance.model.AuthenticationRequest;
import com.milankas.training.userapi.persistance.model.AuthenticationResponse;
import com.milankas.training.userapi.services.MyUserDetailsService;
import com.milankas.training.userapi.services.UserService;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class UserController {

    private LogStatusDto logStatusDto = new LogStatusDto();

    private AuthenticationManager authenticationManager;
    private UserService userService;
    private RestTemplate restTemplate;
    private MyUserDetailsService userDetailsService;
    private JwtUtils jwtUtils;
    private RabbitTemplate template;

    public UserController(RabbitTemplate template, UserService userService, RestTemplate restTemplate, AuthenticationManager authenticationManager, MyUserDetailsService userDetailsService, JwtUtils jwtUtils) {
        this.userService = userService;
        this.restTemplate = restTemplate;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtUtils = jwtUtils;
        this.template = template;
    }

    @GetMapping("/v1/users")
    public List<UserDto> getAllUsers() {
        logStatusDto = userService.buildLogStatusDto("info", "get users list");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return userService.getUsers();
    }

    @GetMapping("/v1/users/{userId}")
    public UserDto getUserById(@PathVariable UUID userId){
        if (userService.getUser(userId) == null) {
            logStatusDto = userService.buildLogStatusDto("error", "get user by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }
        else {
            logStatusDto = userService.buildLogStatusDto("info", "get user by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return userService.getUser(userId);
        }
    }

    @GetMapping("/v1/users/healthcheck")
    public String healthCheck() {
        logStatusDto = userService.buildLogStatusDto("info", "get healthcheck");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return "I'm Alive";
    }

    @PostMapping("/v1/users")
    public ResponseEntity<Object> postUser(@Valid @RequestBody UserInDto userInDto, BindingResult result){
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = userService.buildLogStatusDto("error", "adding new user");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            if (userService.validEmail(userInDto.getEmail()) == false) {
                JSONObject message = new JSONObject();
                message.put("status", "400");
                message.put("error", "Bad request");
                message.put("message", "Duplicate email");
                logStatusDto = userService.buildLogStatusDto("error", "adding new user");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.badRequest().body(message.toString());
            }
            else {
                logStatusDto = userService.buildLogStatusDto("info", "adding new user");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.ok(userService.saveUser(userInDto));
            }
        }
    }

    @PostMapping("/v1/users/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        if (userService.correctUser(authenticationRequest)) {
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(authenticationRequest.getUsername());

            final String jwt = jwtUtils.generateToken(userDetails);
            logStatusDto = userService.buildLogStatusDto("info", "authenticate user");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.ok(new AuthenticationResponse(jwt));
        }
        else {
            JSONObject message = new JSONObject();
            message.put("status", "400");
            message.put("error", "Bad request");
            message.put("message", "Invalid User");
            logStatusDto = userService.buildLogStatusDto("error", "authenticate user");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return new ResponseEntity<>(message.toString(), HttpStatus.FORBIDDEN);
        }

    }

    @DeleteMapping("/v1/users/{userId}")
    public ResponseEntity deleteUser(@PathVariable UUID userId) {
        if (userService.deleteUser(userId) == null) {
            logStatusDto = userService.buildLogStatusDto("error", "deleting user");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user not found"
            );
        }
        else {
            logStatusDto = userService.buildLogStatusDto("info", "deleting user");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PatchMapping("/v1/users/{userId}")
    public ResponseEntity<Object> patchUser(@PathVariable UUID userId,@Valid @RequestBody UserPatchDto userPatchDto, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = userService.buildLogStatusDto("error", "updating user");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            if (userService.getUser(userId) == null) {
                logStatusDto = userService.buildLogStatusDto("error", "updating user");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.notFound().build();
            }
            else {
                logStatusDto = userService.buildLogStatusDto("info", "updating user");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.ok(userService.updateUser(userId, userPatchDto));
            }
        }
    }

    @PatchMapping("/v1/users/{userId}/change-password")
    public ResponseEntity<?> changePassword(@PathVariable UUID userId, @Valid @RequestBody NewPassword newPassword,  BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = userService.buildLogStatusDto("error", "updating user password");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation Failure", errors));
        }
        else {
            if (userService.getUser(userId) == null) {
                logStatusDto = userService.buildLogStatusDto("error", "updating user password");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                return ResponseEntity.notFound().build();
            }
            else {
                logStatusDto = userService.buildLogStatusDto("info", "updating user password");
                template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
                JSONObject message = new JSONObject();
                message.put("message", userService.updatePassword(userId, newPassword));
                return ResponseEntity.ok(message.toString());
            }
        }
    }
}
