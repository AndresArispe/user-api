package com.milankas.training.userapi.services;

import com.milankas.training.userapi.dto.in.NewPassword;
import com.milankas.training.userapi.dto.in.UserInDto;
import com.milankas.training.userapi.dto.log.LogStatusDto;
import com.milankas.training.userapi.dto.out.UserDto;
import com.milankas.training.userapi.dto.patch.UserPatchDto;
import com.milankas.training.userapi.mappers.PasswordMapper;
import com.milankas.training.userapi.mappers.UserMapper;
import com.milankas.training.userapi.persistance.model.AuthenticationRequest;
import com.milankas.training.userapi.persistance.model.Password;
import com.milankas.training.userapi.persistance.model.User;
import com.milankas.training.userapi.persistance.repository.PasswordRepository;
import com.milankas.training.userapi.persistance.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class UserService {

    private PasswordRepository passwordRepository;
    private UserRepository userRepository;
    private PasswordMapper passwordMapper;
    private UserMapper userMapper;

    public UserService(PasswordRepository passwordRepository, UserRepository userRepository, PasswordMapper passwordMapper, UserMapper userMapper) {
        this.passwordRepository = passwordRepository;
        this.userRepository = userRepository;
        this.passwordMapper = passwordMapper;
        this.userMapper = userMapper;
    }

    public List<UserDto> getUsers() {
        return userMapper.toUsersDto(userRepository.findAll());
    }

    public UserDto getUser(UUID id) {
        return userMapper.userToDto(userRepository.findById(id).orElse(null));
    }

    public UserDto saveUser(UserInDto userInDto) {
        User newUser = new User();
        newUser.setFirstName(userInDto.getFirstName());
        newUser.setLastName(userInDto.getLastName());
        newUser.setEmail(userInDto.getEmail());
        Password newPassword = new Password();
        newPassword.setStatus(1);
        String salt = DatatypeConverter.printHexBinary(generateSalt()).toLowerCase();
        newPassword.setSalt(salt);
        salt = salt + userInDto.getPassword();
        String hash = generateHash(salt.getBytes(), "SHA-256");
        newPassword.setHash(hash);
        newPassword.setUser(newUser);
        newUser.getPasswords().add(newPassword);
        userRepository.save(newUser);
        return userMapper.userToDto(newUser);
    }

    public Boolean validEmail(String email) {
        User user = userRepository.getUserByEmail(email);
        if (user != null) {
            return false;
        }
        return true;
    }

    public byte[] generateSalt() {
        Random r = new SecureRandom();
        byte[] salt = new byte[8];
        r.nextBytes(salt);
        return salt;
    }

    public String generateHash(byte[] inputBytes, String algorithm) {
        String hashValue = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(inputBytes);
            byte[] digestedBytes = messageDigest.digest();
            hashValue = DatatypeConverter.printHexBinary(digestedBytes).toLowerCase();
        } catch (Exception e) {

        }
        return hashValue;
    }

    public UserDto deleteUser(UUID userId) {
        User deletedUser = userRepository.findById(userId).orElse(null);
        if (deletedUser == null) {
            return null;
        }
        else {
            userRepository.deleteById(userId);
            return userMapper.userToDto(deletedUser);
        }
    }

    public UserDto updateUser(UUID userId, UserPatchDto userPatchDto) {
        User existingUser = userRepository.findById(userId).orElse(null);
        if (existingUser == null) {
            return null;
        }
        else {
            if (userPatchDto.getFirstName() != null) {
                existingUser.setFirstName(userPatchDto.getFirstName());
            }
            if (userPatchDto.getLastName() != null) {
                existingUser.setLastName(userPatchDto.getLastName());
            }
            if (userPatchDto.getEmail() != null) {
                existingUser.setEmail(userPatchDto.getEmail());
            }
            if (userPatchDto.getPassword() != null) {
                for(int i = 0; i < existingUser.getPasswords().size(); i++) {
                    existingUser.getPasswords().get(i).setStatus(0);
                }
                Password newPassword = new Password();
                newPassword.setStatus(1);
                String salt = DatatypeConverter.printHexBinary(generateSalt()).toLowerCase();
                newPassword.setSalt(salt);
                salt = salt + userPatchDto.getPassword();
                String hash = generateHash(salt.getBytes(), "SHA-256");
                newPassword.setHash(hash);
                existingUser.getPasswords().add(newPassword);
                userRepository.save(existingUser);
            }
        }
        return userMapper.userToDto(userRepository.save(existingUser));
    }


    public boolean correctUser(AuthenticationRequest authenticationRequest) {
        List<User> allUsers = userRepository.findAll();
        User foundUser = allUsers.stream()
                .filter(user -> user.getEmail().equals(authenticationRequest.getUsername()))
                .findAny()
                .orElse(null);
        if ((foundUser != null) && comprobePassword(foundUser, authenticationRequest.getPassword())) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean comprobePassword(User foundUser, String password) {
        Password userPassword = foundUser.getPasswords().stream()
                .filter(password1 -> password1.getStatus() == 1)
                .findAny()
                .orElse(null);
        String salt = userPassword.getSalt() + password;
        String hash = generateHash(salt.getBytes(), "SHA-256");
        if (hash.equals(userPassword.getHash())) {
            return true;
        }
        else {
            return false;
        }
    }

    public String updatePassword(UUID userId, NewPassword newPassword) {
        User existingUser = userRepository.findById(userId).orElse(null);
        Password existingPassword = existingUser.getPasswords().stream()
                .filter(password -> password.getStatus() == 1)
                .findAny()
                .orElse(null);
        String salt = existingPassword.getSalt() + newPassword.getActualPassword();
        String hash = generateHash(salt.getBytes(), "SHA-256");
        if (hash.equals(existingPassword.getHash())) {
            for(int i = 0; i < existingUser.getPasswords().size(); i++) {
                existingUser.getPasswords().get(i).setStatus(0);
            }
            Password createPassword = new Password();
            createPassword.setStatus(1);
            String createSalt = DatatypeConverter.printHexBinary(generateSalt()).toLowerCase();
            createPassword.setSalt(createSalt);
            createSalt = createSalt + newPassword.getNewPassword();
            String createHash = generateHash(createSalt.getBytes(), "SHA-256");
            createPassword.setHash(createHash);
            existingUser.getPasswords().add(createPassword);
            userRepository.save(existingUser);
            return "Password changed";
        }
        else {
            return "Actual password it's incorrect";
        }
    }

    public LogStatusDto buildLogStatusDto(String level, String message) {
        LogStatusDto logStatusDto = new LogStatusDto();
        logStatusDto.setMessage(message);
        logStatusDto.setLevel(level);
        logStatusDto.setDateAndTime(new Date());
        logStatusDto.setServiceName("User-API");
        return logStatusDto;
    }
}
