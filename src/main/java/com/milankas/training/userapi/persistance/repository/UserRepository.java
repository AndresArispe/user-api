package com.milankas.training.userapi.persistance.repository;

import com.milankas.training.userapi.persistance.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository <User, UUID> {

    @Query(
            value = "SELECT * FROM user_table u where u.email = :email",
            nativeQuery = true)
    public User getUserByEmail(@Param("email") String email);
}
