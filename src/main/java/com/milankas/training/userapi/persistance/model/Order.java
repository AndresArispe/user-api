package com.milankas.training.userapi.persistance.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class Order {

    private UUID id;

    private UUID userId;
    private String emailAddress;
    private Address address;
    private List<LineItem> lineItems = new ArrayList<>();

}
