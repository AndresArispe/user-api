package com.milankas.training.userapi.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

public class Address {

    private UUID id;

    private String addressLine1;
    private String addressLine2;
    private String contactName;
    private String contactPhoneNumber;
    private String state;
    private String city;
    private String zipCode;
    private String countryCode;
}
