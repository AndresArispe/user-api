package com.milankas.training.userapi.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

public class LineItem {

    private UUID lineItemId;

    private UUID productId;
    private int qty;

}
