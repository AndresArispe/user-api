FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ENV APP_PORT=8093
ENV DB_URL=jdbc:postgresql://postgres-users:5432/user
ENV DB_USER=postgres
ENV DB_PASSWORD=godis1first
ARG JAR_FILE=target/*.jar
ARG RSA=*.pem
ADD ${JAR_FILE} api.jar
ADD ${RSA} /
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","api.jar"]